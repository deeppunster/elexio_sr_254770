"""
Sample python program to support ticket SR:254770.

This program requires Python 3.6 or newer and installing the library
"requests" from https://pypi.org/.
"""
import requests as rq

import json
import sys
from traceback import print_exception
from pprint import pprint, pformat
from typing import Any, Union, Optional


class ElexioAPIClass:
    """
    ElexioAPIClass - Access Elexio via API.
    """

    def __init__(self):
        """
        Establish internal variables.
        """
        # common URL prefix for all requests
        self.URL_prefix = "https://chmaster.elexiochms.com/api/v2"

        # session info

        self.userid: str = ""
        self.password: str = ""
        self.sessionid: str = ""
        self.decoder = json.JSONDecoder()
        self.file_prefix = "ElexioData-"

        # self.userid = "<redacted>"
        # self.password = "<redacted>"
        # self.person_uid = '<redacted>'
        return

    def run_E_API(self):
        """
        Use various API calls for accessing Elexio.

        """
        print("Starting to use Elexio APIs")
        try:
            print("Login to Elexio")
            self.login_request()

            # get a group list
            group_list = self.get_group_list()

            # get_people_list
            people_list = self.get_people_list()

            person = self.get_a_person(uid=self.person_uid)
            update_dict = self.modify_person(person)
            self.update_person(
                uid=self.person_uid, person_dict=person, update_dict=update_dict
            )
        except Exception as ex:
            ex_type, ex_val, ex_trace = sys.exc_info()
            print_exception(ex_type, ex_val, ex_trace)
            print(f"Logging out of Elexio...")
        self.Logout()
        print(f"Shutting down...")
        return

    def login_request(self):
        """
        Present credentials and set the token for this session.

        Note: the token is stored in a class instance variable for later use.
        """

        login_url = self.URL_prefix + "/user/login"

        login_payload = dict()
        login_payload["username"] = self.userid
        login_payload["password"] = self.password

        login_headers = dict()
        login_headers["Content - Type"] = "application / x - www - form - urlencoded"

        print("\nSending login request...")
        response = rq.post(login_url, data=login_payload, headers=login_headers)
        print(f"Status: {response.status_code} - {response.reason}")

        # the data is a single dictionary
        login_dict = self.report_response(response=response, header="Login")

        self.sessionid = login_dict["session_id"]
        return

    def get_group_list(self) -> list:
        """
        Get the list of groups in Elexio.

        :return: a list of dictionaries, one for each group
        """
        groups_url = self.URL_prefix + "/groups"
        group_label = "Group List"
        group_file_name = self.file_prefix + group_label + ".txt"

        groups_payload = dict()
        groups_payload["session_id"] = self.sessionid

        print("\nSending group list request...")
        response = rq.get(groups_url, data=groups_payload)

        # the data is a list of dictionaries
        group_list = self.report_response(
            response=response, header=group_label, report_data=False
        )

        with open(group_file_name, mode="w") as group_file:
            group_file.write(pformat(group_list, compact=True))

        return group_list

    def get_people_list(self) -> dict:
        """
        Get the list of people in Elexio.

        :return: a list of people (as a dict)
        """
        people_url = self.URL_prefix + "/people"
        people_label = "People List"
        people_file_name = self.file_prefix + people_label + ".txt"

        people_payload = dict()
        people_payload["session_id"] = self.sessionid

        print("\nSending group list request...")
        response = rq.get(people_url, data=people_payload)

        # the data is a list of dictionaries
        people_list = self.report_response(
            response=response, header="People List", report_data=False
        )

        with open(people_file_name, mode="w") as people_file:
            people_file.write(pformat(people_list, compact=True))

        return people_list

    def get_a_person(self, uid: str) -> Optional[dict]:
        """
        Get a person by UID and return that person's information.

        :param uid: Elexio's magic number for the requested person
        :return: a dictionary of the fields for the requested person
        """
        person_url = self.URL_prefix + f"/people/{uid}"
        person_label = f"Person {uid} Dictionary"
        person_file_name = self.file_prefix + person_label + ".txt"

        person_payload = dict()
        person_payload["session_id"] = self.sessionid

        print(f"\nSending request for info about {uid}...")
        response = rq.get(person_url, data=person_payload)

        # the data is a dictionary
        person_dict = self.report_response(
            response=response, header=person_label, report_data=True
        )

        with open(person_file_name, mode="w") as person_file:
            person_file.write(pformat(person_dict, compact=False))

        return person_dict

    def modify_person(self, person: dict) -> dict:
        """
        Modify a person's information.

        :param person: dictionary of person's current info
        :return: dictionary of changes to be made
        """
        change_dict = dict()

        # must always include the persons uid
        change_dict["uid"] = person["uid"]

        # schedule desired changes
        change_dict["text15"] = "Middle Name"
        return change_dict

    def update_person(self, uid: str, person_dict: dict, update_dict: dict):
        """
        Update the information for a person
        :param uid: the user id for the person to be changed
        :param person_dict: dictionary of information about the person
        """

        update_url = self.URL_prefix + f"/people/{uid}"
        update_label = (
            f"Updating {person_dict['fname']} " f"{person_dict['lname']} ({uid})"
        )
        update_payload = update_dict
        update_payload["session_id"] = self.sessionid

        print(update_label)
        response = rq.put(update_url, data=update_payload)

        # report results
        self.report_response(response=response, header=update_label, report_data=True)

        return

    def Logout(self):
        """
        Invalidate the token for this session.
:
        """

        # logout url
        logout_url = self.URL_prefix + "/user/logout"

        logout_payload = dict()
        logout_payload["session_id"] = self.sessionid
        logout_payload["logout"] = "true"

        print("\nSending logout request...")
        response = rq.post(logout_url, data=logout_payload)

        print(f"Logout response: {response.text}")
        return

    def report_response(
        self, response: rq.Response, header: str = None, report_data: bool = True
    ) -> Union[list, dict]:
        """
        Report generically the response to an Elexio API request.

        :param response:
        :param header: declaration of the type of dictionary
        :param report_data: print out the data contents?
        :return: data contents
        """
        if header:
            print(f"\n{header}:")
        print(f"Status: {response.status_code} - {response.reason}")

        # note - response.headers is a CaseInsenitiveOrderedDict
        response_header_dict = response.headers._store
        self.print_dict(response_dict=response_header_dict, label="Headers")
        print(f"Content: {response.content}")
        response_text = response.text
        response_dict = self.response_text_to_dict(response_text)
        response_data = response_dict["data"]
        response_success = response_dict["success"]
        print(f"Success? {response_success}")
        if report_data:
            print("Contents of data:")
            self.print_dict(response_dict=response_data)
        return response_data

    def response_text_to_dict(self, response_text: str) -> Union[dict, list]:
        """
        extract the values into a dictionary using json.

        :param response_text: string of text from the body of the response.
        :return: the (possibly recursively built) dictionary
        """
        """
        sample input string (formatted):
         {"data":
            {"fname":"Travis",
             "lname":"Risner",
             "uid":214291,
             "session_id":"ebc890ca491bbfdaa698d1fc60daaa44",
             "org_name":"Church of the Master UM"
            },
          "success":true
         }
        """
        rt = response_text

        # a sanity check
        if rt.startswith("{") and rt.endswith("}"):
            pass
        else:
            raise ValueError(f'Response "{rt}" missing one or both outer ' f"braces")

        response_dict = self.decoder.decode(response_text)
        return response_dict

    def print_dict(self, response_dict: dict, label: str = None):
        """
        Print the contents of a response dictionary.

        :param response_dict: dictionary to be printed
        :param label: optional label for dictionary
        """
        if label:
            print(f"\n{label}:")
        pprint(response_dict)

        return


if __name__ == "__main__":
    E_API = ElexioAPIClass()
    print("Starting up E_API")
    E_API.run_E_API()
    print("End of Elexio API session")

# EOF
